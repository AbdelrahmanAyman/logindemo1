//
//  User.swift
//  logInDemo
//
//  Created by IFTS23 on 18/05/2020.
//  Copyright © 2020 IFTS23. All rights reserved.
//

import UIKit


struct User {
    
    var userName :String
    var email :String
    var password : String
    var phoneNumber :String
    var adress : String 
    
    init(name: String,email :String ,pass: String ,phoneNumber: String,adress:String)  {
        self.userName = name
        self.email = email
        self.password = pass
        self.adress = adress
       
        self.phoneNumber = phoneNumber
        
    }
    
}


