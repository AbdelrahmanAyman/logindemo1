//
//  SignUpViewController.swift
//  logInDemo
//
//  Created by IFTS23 on 18/05/2020.
//  Copyright © 2020 IFTS23. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {

    
    
    @IBOutlet weak var signUpButtom: UIButton!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var adduserPhoto: UIButton!
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var emailTaxt: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var userAdress: UITextField!
    @IBOutlet weak var userGender: UILabel!
    @IBOutlet weak var selectGender: UISwitch!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Sign Up"
        signUpButtom.layer.cornerRadius = signUpButtom.frame.height / 2
       
    }

    
    
    @IBAction func signUpButtom(_ sender: UIButton) {
        guard let  email =  emailTaxt.text , !email.isEmpty , let pass = password.text , !pass.isEmpty else {
           
            
            // create new Alert
            
            let dialogMessage = UIAlertController(title: "Attention!", message: "Email and Password are requird.", preferredStyle: UIAlertController.Style.alert)
            // create ok button
            let ok =  UIAlertAction(title: "ok", style: .default) { (action) -> Void in
                print("ok button tapped")
            }
            // add ok to dialog message
            dialogMessage.addAction(ok)
            
            self.present(dialogMessage,animated: true,completion: nil)

               return
        }
      
        let LogInViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
        
        let user  = User(name: userName.text ?? "", email: emailTaxt.text ?? "", pass: password.text ?? "", phoneNumber: phoneNumber.text ?? "", adress: userAdress.text ?? "" )
        
        LogInViewController.user = user
        self.navigationController?.pushViewController(LogInViewController, animated: true)
        
        
    }
    
    @IBAction func swichGender(_ sender: UISwitch) {
        if sender.isOn {
            userGender.text = "Gender: Male"
        }else{
             userGender.text = "Gender: Female"
        }
        
    }
    
    
    @IBAction func addUserPhoto(_ sender: UIButton) {
        let image = UIImagePickerController()
        image.delegate = self
        image.sourceType =  UIImagePickerController.SourceType.photoLibrary
        image.allowsEditing = false
        self.present(image,animated: true)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let selectedImage = info[.originalImage] as? UIImage else {
            fatalError("Expecting a dictionary containing an image but found: \(info )")
            
        }
        userImage.image = selectedImage
        dismiss(animated: true, completion: nil)
    }
}
